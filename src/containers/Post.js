import React from 'react'
import { withRouteData, Link } from 'react-static'
import Moment from 'react-moment'
import Markdown from 'react-markdown'
import { Button } from 'reactstrap';
//

export default withRouteData(({ post }) => (
  <div className="blog-post">
    <Link to="/blog/">{'<'} Back</Link>
    <br />
    <h3>Evo šta ima novo:</h3>
    <Button style={{ width: '100px'}} color="success">Success!</Button>
    <Button style={{ width: '100px'}} className="btn btn-danger float-right">Floated</Button>
    <h2 style={{ textAlign: 'center', color:'green'}}>Konačno radim: css-in-js</h2>
    <h2 style={{ textAlign: 'center', color:'green'}}>{post.data.title}</h2>
    <Moment style={{ textAlign: 'center', color:'green'}} format="MMMM Do, YYYY">{post.data.date}</Moment>
    <img className="image" src={post.data.thumbnail} alt="" />
    <Markdown source={post.content} escapeHtml={false} />
  </div>
))
