import React from 'react'
import { withSiteData } from 'react-static'
import { Button } from 'reactstrap';

//
import logoImg from '../logo.png'

export default withSiteData(() => (
  <div>
    <h1 style={{ textAlign: 'right', color: '#999999', textSize:'500', paddingRight: '100px'}}>Ljudi, ovo je fenomenalno</h1>
    <img src={logoImg} alt="" />
    <Button color="success">Big Success! {'\u2728'}</Button>
    <p>{'\u2728'} Sljedeče: prenijeti bloke iz Mobirise ovamo!</p>
    <Button className="btn btn-success float-right">Right 🧒🏼</Button>
  </div>
))
