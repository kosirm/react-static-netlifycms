---
layout: blog
title: 'Step 3: Ory editor (ili GrapesJS editor...)'
date: 2018-09-24T13:30:12.214Z
thumbnail: 'https://ucarecdn.com/f3624d0e-74ba-4110-90b4-62337bfcab8c/'
rating: '5'
extra: Ovdje če mi trebati pomoč.
---
Ovo je zadnji korak. Upotrijebiti wysiwyg editor za dinamične kompozicije na stranicama.

**Ali do toga je sada predaleko, dok ovo totalno radi... Zašto ne krenuti sa time onako kako je?**
